console.log("Hi fb-api");
var PAGE_ID =null;
var PAGE_TOKEN =null;
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
      testAPI();
    } else {
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '252704232024020',
        cookie     : true,  // enable cookies to allow the server to access 
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v3.2' // The Graph API version to use for the call
    });
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
};
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=id,name,accounts' ,function(response) {
        console.log(response);
        $("#name").html(response.name)
        $("#username").html(response.name);
        sel = "";
        sel += response.accounts.data.map((a,i)=>{
        return '<option value="'+i+'">'+a.name+'</option>';
        });
        $("#pages").html(sel);
        $("#ok").click(()=>{
        var index = $("#pages").val();
        PAGE_ID = response.accounts.data[index].id;
        PAGE_TOKEN = response.accounts.data[index].access_token;
        mypages_ig(PAGE_ID,PAGE_TOKEN);
        mypages_fb(PAGE_ID);
        });
    });
}
  
function  mypages_ig(id,token){
    FB.api('/'+id+'?fields=about,instagram_accounts,instagram_business_account',{access_token : token} ,function(response) {
        console.log(response);
        if(!response.instagram_business_account){
        $("#cards").html("<p class=\"text-center\"><span style=\"color:red\">Must open the \"instagram business account\" first</span></p>");
        return;
        }
        FB.api('/'+response.instagram_business_account.id+'?fields=followers_count,media_count,follows_count,media{caption,like_count,media_url,media_type,comments_count},name',{access_token : token} ,function(response) {
        console.log(response);
        $("#ig_name").html(response.name);
        $("#follow").html(response.followers_count);
        $("#following").html(response.follows_count);
        $("#media_count").html(response.media_count);
        var tag = "";
        for(var i=0;i<response.media.data.length;i++){
            var m = response.media.data[i];
            tag += '<div class="col-md-8 col-md-offset-2">';
            tag += '  <div class="thumbnail">';
            tag += '    <a href="'+m.media_url+'" target="_blank">';
            if(m.media_type != "VIDEO")
            tag += '      <img src="'+m.media_url+'" alt="Fjords" style="width:100%">';
            else {
            //tag += '       <video src="'+m.media_url+'" style="width:100%"></video>';
            tag+='<video width="100%" controls>'; 
            tag+='  <source src="'+m.media_url+'" type="video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\"">';
            tag+='  <source src="'+m.media_url+'" type="video/ogg; codecs=\"theora, vorbis\"">';
            tag+='</video>';
            }
            tag += '      <div class="caption">';
            tag += '        <p>'+m.caption+'</p><span class="glyphicon glyphicon-heart">'+m.like_count+'</span>';
            tag += '        <span class="glyphicon glyphicon-comment">'+m.comments_count+'</span>'
            tag += '      </div>';
            tag += '    </a>';
            if(m.comments_count > 0) 
            tag += '      <p><small><a>comments..</small></a></p>';
            tag += '  </div>';
            tag += '</div>';
        }
        $("#cards").html(tag);
        });
    });
}
function  mypages_fb(id){
    FB.api('/'+id+'?fields=country_page_likes,posts{message,permalink_url,status_type,full_picture,comments,likes},name,about',function(response) {
        console.log(response);
        $("#fb_page").html(response.name);
        $("#fb_like").html(response.country_page_likes);
        $("#fb_post").html(response.posts.data.length);
        $("#fb_about").html(response.about);
        var tag = "";
        for(var i=0;i<response.posts.data.length;i++){
        var m = response.posts.data[i];
        var comm_count = m.comments ? m.comments.data.length : 0;
        var lk_count = m.likes ? m.likes.data.length : 0;
        tag += '<div class="col-md-12"> ';
        tag += '  <div class="thumbnail">';
        tag += '<i class="glyphicon glyphicon-trash pull-right" onclick="post_fb_del('+ "'" +m.id+"'" +')"></i>'
        tag += '    <a href="'+m.permalink_url+'" target="_blank">';
        if(m.status_type=="added_photos")
            tag += '      <img src="'+m.full_picture+'" alt="Fjords" style="width:100%">';
        else {
            
        }
        tag += '      <div class="caption">';
        tag += '        <p>'+m.message+'</p><span class="glyphicon glyphicon-heart">'+lk_count+'</span>';
        tag += '        <span class="glyphicon glyphicon-comment">'+comm_count+'</span>'
        tag += '      </div>';
        tag += '    </a>';
        if(comm_count > 0) 
            tag += '      <p><small><a>comments..</small></a></p>';
        tag += '  </div>';
        tag += '</div>';
        }
        
        $("#fb_cards").html(tag);
    });
}

function post_submit(){
    var mss = $("#post_message").val();
    var psh = $("#publish").is(":checked");
    var data = {
        message : mss
    };
    if(PAGE_ID)
        post_fb_album(data)
        // post_fb_img(data)
        // post_fb(data);
}
function post_fb(data){
    FB.api(
        "/"+PAGE_ID+"/feed",
        "POST",
        {
            "message": data.message,
            "access_token" : PAGE_TOKEN
        },
        function (response) {
          if (response && !response.error) {
            /* handle the result */
            $("#post_review").html("Post message : \""+ data.message+"\" success!");
            mypages_fb(PAGE_ID);
          }
        }
    );
}
function post_fb_del(id){
    FB.api(
        "/"+id,
        "DELETE",
        {"access_token" : PAGE_TOKEN},
        function (response) {
          if (response && !response.error) {
            mypages_fb(PAGE_ID);
          }
        }
    );
}
function post_fb_img(data){
    FB.api(
        "/"+PAGE_ID+"/photos",
        "POST",
        {
            "url": "https://scontent.fbkk22-2.fna.fbcdn.net/v/t1.0-9/52333456_2153216921401504_1333302438752419840_o.jpg?_nc_cat=103&_nc_ht=scontent.fbkk22-2.fna&oh=b266a4859d3a909adaf50e257b1d3764&oe=5CEDB7F9",
            "caption":data.message
        },
        function (response) {
          if (response && !response.error) {
            /* handle the result */
          }
        }
    );
}
function post_fb_share(){
    FB.ui({
        method: 'share',
        href: 'https://developers.facebook.com/docs/',
    }, function(response){});
}
function  post_fb_album(data){
    FB.api(
        "/"+PAGE_ID+"/albums",
        "POST",
        {
            "name": "My Album "+ new Date(),
            "access_token" : PAGE_TOKEN,
            "description" : "For test API LOL"
        },
        function (response) {
          if (response && !response.error) {
            var messages = ["m1","m2","m3","m4"];
            messages.map((m)=>{
                FB.api(
                    "/"+response.id+"/photos",
                    "POST",
                    {
                        "url": "https://scontent.fbkk22-2.fna.fbcdn.net/v/t1.0-9/52333456_2153216921401504_1333302438752419840_o.jpg?_nc_cat=103&_nc_ht=scontent.fbkk22-2.fna&oh=b266a4859d3a909adaf50e257b1d3764&oe=5CEDB7F9",
                        "access_token" : PAGE_TOKEN,
                        "message":data.message + m
                    },
                    function (response) {
                      if (response && !response.error) {
                        
                      }
                    }
                );
            });
          }
        }
    );
    
}
function  post_fb_album_add_photo(data){
    FB.api(
        "/416177405858426/photos",
        "POST",
        {
            "url": "https://images.unsplash.com/photo-1483691278019-cb7253bee49f?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
            "access_token" : PAGE_TOKEN,
            "message":data.message
        },
        function (response) {
            if (response && !response.error) {
                mypages_fb(PAGE_ID);
            }
        }
    );
          

    
}