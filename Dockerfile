FROM node:lts-slim
RUN apt-get update && apt-get install -y sudo \
    && apt-get install -y nano openssh-server net-tools \
    && usermod -a -G sudo node \
    && npm install -g pm2 tsc
WORKDIR /home/node/sourcecode
COPY . /home/node/sourcecode
RUN chown -R node:node /home/node
RUN npm install
CMD ["npm","start"]